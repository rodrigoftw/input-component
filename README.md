# Input Component Page #

## Solution for a challenge from [Devchallenges.io](http://devchallenges.io). ##


## [Demo](https://kind-fermat-3abe9e.netlify.app/) | [Solution](https://devchallenges.io/solutions/f1wSKZELdsw53Z4I9Rsp) | [Challenge](https://devchallenges.io/challenges/TSqutYM4c5WtluM7QzGp) ##

## Table of Contents

- [Overview](#overview)
- [Built With](#built-with)
- [Features](#features)
- [Contact](#contact)

## Overview

![screenshot](https://devchallenges.io/_next/image?url=https%3A%2F%2Ffirebasestorage.googleapis.com%2Fv0%2Fb%2Fdevchallenges-1234.appspot.com%2Fo%2FchallengesDesigns%252FInputThumbnail.png%3Falt%3Dmedia%26token%3D73685593-9026-42a6-8c68-00243071250e&w=1920&q=75)

This application/site was created as a submission to a [DevChallenges](https://devchallenges.io/challenges) challenge.
The [challenge](https://devchallenges.io/challenges/TSqutYM4c5WtluM7QzGp) was to build an application to complete the given user stories.

## Built With

- [ReactJS](https://reactjs.org/)
- [Styled Components](https://styled-components.com/)

## Features

This project features a responsive page showcasing an Input Component with several states and props. Built with ReactJS and Styled Components.

## Contact

You can contact me [here](https://linktr.ee/rodrigodev).
