import styled from 'styled-components';

export const Container = styled.div`
  max-height: 42px;
  height: 100%;
  width: auto;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  margin: 0 0 24px 80px;

  @media(max-width: 768px) {
    justify-content: center;
    text-align: center;
    margin: 0 0 24px 0;
  }
`;

export const FooterTitle = styled.span`
  font-family: "Montserrat", sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;

  color: #A9A9A9;
`;

export const FooterLink = styled.span`
  font-family: "Montserrat", sans-serif;
  font-style: normal;
  font-weight: 700;
  font-size: 14px;
  line-height: 17px;
  cursor: pointer;
  margin: 0 4px;
  color: #A9A9A9;
  text-decoration: underline;
`;
