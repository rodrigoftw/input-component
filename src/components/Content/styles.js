import styled from 'styled-components';

export const Container = styled.div`
  height: 100%;
  /* max-width: 786px; */
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  justify-content: center;
  flex-grow: 1;
`;

export const Row = styled.div`
  height: auto;
  width: auto;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  margin: 24px 80px;
  /* margin: 24px 0 24px 80px; */

  @media(max-width: 768px) {
    margin: 24px;
    flex-wrap: wrap;
    justify-content: center;
  }
`;

export const Column = styled.div`
  height: 123px;
  width: 100%;
  min-width: 200px;
  max-width: 100%;
  
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;
  margin-right: 48px;

  @media(max-width: 768px) {
    min-width: 100%;
    max-width: 100%;
    width: 100%;
    margin: 0;
  }
`;

export const TitleColumn = styled.div`
  height: 36px;
  width: 100%;
  padding: 53px 0 32px;

  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-evenly;

  @media(max-width: 768px) {
    width: 100%;
    align-items: center;
    justify-content: center;
  }
`;

export const Title = styled.span`
  font-family: "Poppins", sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 36px;

  color: #4F4F4F;
`;

export const InputCodeTitle = styled.span`
  font-family: "Ubuntu Mono", monospace;
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 12px;

  color: #333333;
`;

export const InputInfoTitle = styled.span`
  font-family: "Ubuntu Mono", monospace;
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 12px;
  text-align: center;

  color: #828282;
`;