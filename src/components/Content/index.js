import React from 'react';
import Input from "../Input";
import Footer from "../Footer";

import {
  Container,
  Row,
  Column,
  TitleColumn,
  Title,
  InputCodeTitle,
  InputInfoTitle,
} from './styles';

function Content() {
  return (
    <Container>
      <Row>
        <TitleColumn>
          <Title>Inputs</Title>
        </TitleColumn>
      </Row>
      <Row>
        <Column>
          <InputCodeTitle>{"<Input />"}</InputCodeTitle>
          <Input placeholder="Placeholder" />
        </Column>
        <Column>
          <InputInfoTitle>{"&:hover"}</InputInfoTitle>
          <Input hovered placeholder="Placeholder" />
        </Column>
        <Column>
          <InputInfoTitle>{"&:focus"}</InputInfoTitle>
          <Input focused placeholder="Placeholder" />
        </Column>
      </Row>
      <Row>
        <Column>
          <InputCodeTitle>{"<Input error />"}</InputCodeTitle>
          <Input error placeholder="Placeholder" />
        </Column>
        <Column>
          <InputInfoTitle>{"&:hover"}</InputInfoTitle>
          <Input hovered placeholder="Placeholder" />
        </Column>
        <Column>
          <InputInfoTitle>{"&:focus"}</InputInfoTitle>
          <Input error placeholder="Placeholder" />
        </Column>
      </Row>
      <Row>
        <Column>
          <InputCodeTitle>{"<Input disabled />"}</InputCodeTitle>
          <Input disabled placeholder="Placeholder" />
        </Column>
      </Row>
      <Row>
        <Column>
          <InputCodeTitle>{`<Input helperText="Some interesting text" />`}</InputCodeTitle>
          <Input helperText="Some interesting text" placeholder="Placeholder" />
        </Column>
        <Column>
          <InputCodeTitle>{`<Input helperText="Some interesting text" error />`}</InputCodeTitle>
          <Input helperText="Some interesting text" error placeholder="Placeholder" />
        </Column>
      </Row>
      <Row>
        <Column>
          <InputCodeTitle>{"<Input startIcon />"}</InputCodeTitle>
          <Input startIcon="call" placeholder="Placeholder" />
        </Column>
        <Column>
          <InputCodeTitle>{"<Input endIcon />"}</InputCodeTitle>
          <Input endIcon="lock" placeholder="Placeholder" />
        </Column>
      </Row>
      <Row>
        <Column>
          <InputCodeTitle>{`<Input value="text" />`}</InputCodeTitle>
          <Input value="Text" placeholder="Placeholder" />
        </Column>
      </Row>
      <Row>
        <Column
        // style={{
        //   justifyContent: "flex-start"
        // }}
        >
          <InputCodeTitle>{`<Input size="sm" />`}</InputCodeTitle>
          <Input size="sm" placeholder="Placeholder" />
        </Column>
        <Column>
          <InputCodeTitle>{`<Input size="md" />`}</InputCodeTitle>
          <Input size="md" placeholder="Placeholder" />
        </Column>
      </Row>
      <Row>
        <Column style={{
          width: "100%", marginRight: "0"
        }}>
          <InputCodeTitle>{"<Input fullWidth />"}</InputCodeTitle>
          <Input fullWidth value="Text" placeholder="Placeholder" />
        </Column>
      </Row>
      <Row>
        <Column
          style={{
            minHeight: "250px",
            maxHeight: "300px",
            height: "100%",
            justifyContent: "space-between"
          }}
        >
          <InputCodeTitle>{`<Input multiline row="4" />`}</InputCodeTitle>
          <Input multiline row="4" placeholder="Placeholder" />
        </Column>
      </Row>


      <Footer />
    </Container>
  );
}

export default Content;