import styled, { css } from 'styled-components';

export const Label = styled.label`
  font-family: "Noto Sans JP", sans-serif;
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 17px;

  color: #333333;
  
  ${(props) =>
    props.error &&
    css`
      color: #D32F2F;
    `
  }
  
  ${(props) =>
    props.hovered &&
    css`
      color: #333333;
    `
  }
  
  ${(props) =>
    props.focused &&
    css`
      color: #2962FF;
    `
  }
`;

export const InputContainer = styled.div`
  width: 200px;
  min-height: 56px;
  max-height: 56px;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  background: transparent;
  border-radius: 8px;
  border: ${props => props.error ? "1px solid #D32F2F" : "1px solid #828282"};
  outline: none;
  transition: color 0.4s;

  :hover {
    border: 1px solid #333333;
  }
  
  :focus,
  :focus-within,
  :focus-visible {
    border: 1px solid #2962FF;
    span.material-icons {
      color: #2962FF;
    }
  }

  ${(props) =>
    props.hovered &&
    css`
      border: 1px solid #333333;
    `
  }
  
  ${(props) =>
    props.focused &&
    css`
      border: 1px solid #2962FF;
    `
  }

  ${(props) =>
    props.disabled &&
    css`
      border: none;
      :hover {
        border: none;
      }
    `
  }
  
  ${(props) =>
    props.size === "sm" &&
    css`
      min-height: 40px;
      max-height: 40px;
    `
  }
  
  ${(props) =>
    props.size === "md" &&
    css`
      min-height: 56px;
      max-height: 56px;
    `
  }
  
  ${(props) =>
    props.fullWidth &&
    css`
      width: 100%;
      max-width: 100%;
    `
  }
  
  ${(props) =>
    props.multiline &&
    css`
      min-height: 150px;
      max-height: 150px;
      padding: 18px 12px;
    `
  }
  
  span {
    color: ${props => props.error ? "#D32F2F" : "#828282"};
  }

  @media(max-width: 768px) {

    ${(props) =>
    !props.multiline &&
    css` 
      min-width: 100%;
      max-width: 100%;
      width: 100%;
      margin: 0;
    `
  }
  }
`;

export const Input = styled.input`
  width: 100%;
  height: 100%;
  padding: 0 12px;
  
  font-family: "Noto Sans JP", sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
  color: #333333;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background: transparent;
  border-radius: 8px;
  border: none;
  /* border: ${props => props.error ? "1px solid #D32F2F" : "1px solid #828282"}; */
  outline: none;
  transition: color 0.4s;

  ::placeholder {
    font-family: "Noto Sans JP", sans-serif;
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 20px;

    color: #828282;
  }

  /* ${(props) =>
    props.focused &&
    css`
      border: 1px solid #2962FF;
    `
  } */

  :disabled {
    background: #F2F2F2;
    border: 1px solid #E0E0E0;
  }
`;

export const TextArea = styled.textarea`
  width: 200px;
  min-height: 150px;
  max-height: 150px;
  height: 150px;
  
  font-family: "Noto Sans JP", sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
  color: #333333;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background: transparent;
  border-radius: 8px;
  border: none;
  outline: none;
  transition: color 0.4s;
  resize: none;

  ::placeholder {
    font-family: "Noto Sans JP", sans-serif;
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 20px;

    color: #828282;
  }

  :disabled {
    background: #F2F2F2;
    border: 1px solid #E0E0E0;
  }
`;

export const InfoText = styled.span`
  font-family: "Noto Sans JP", sans-serif;
  font-style: normal;
  font-weight: 400;
  font-size: 10px;
  line-height: 14px;

  color: ${props => props.error ? "#D32F2F" : "#828282"};
`;