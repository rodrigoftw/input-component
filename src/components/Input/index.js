import React from 'react';

import { Label, InputContainer, Input, TextArea, InfoText } from './styles';

const CustomInput = ({
  placeholder,
  hovered,
  focused,
  error,
  disabled,
  helperText,
  startIcon,
  endIcon,
  value,
  size,
  fullWidth,
  multiline,
  row
}) => {
  return (
    <>
      <Label
        hovered={hovered}
        focused={focused}
        error={error}
      >
        Label
      </Label>
      <InputContainer
        error={error}
        hovered={hovered}
        focused={focused}
        disabled={disabled}
        size={size}
        fullWidth={fullWidth}
        multiline={multiline}
      >
        {startIcon && (
          <span
            className="material-icons"
            style={{ marginLeft: "8px" }}
          >
            {startIcon}
          </span>
        )}
        {!multiline ? (
          <Input
            placeholder={placeholder}
            focused={focused}
            disabled={disabled}
            value={value}
          />
        ) : (
          <TextArea
            placeholder={placeholder}
            focused={focused}
            disabled={disabled}
            value={value}
            row={row}
          />
        )}
        {endIcon && (
          <span
            className="material-icons"
            style={{ marginRight: "8px" }}
          >
            {endIcon}
          </span>
        )}
      </InputContainer>
      <InfoText error={error}>{helperText}</InfoText>
    </>
  );
}

export default CustomInput;